# Hello

## Intro

Just a hello world program.

## How to

```shell
$ go get gitlab.com/mycroft/hello
$ $GOPATH/bin/hello
```
